﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLInterfaces;
using Models;

namespace TestServices
{
    public class TestOrderService: IOrdersService
    {
        public PagedResult<OrderModel> FindMany(int page = 1, int pageLen = 10, string sortBy = "", string sort = "")
        {
            var rr = new PagedResult<OrderModel>();
            var res = new List<OrderModel>();
                       
            res.Add(new OrderModel() { Car = "Lada  2105", ClientID = 3, ClientName = "Василий Черницын", ID = 123, Status = "Created" });
            res.Add(new OrderModel() { Car = "Chevrolet Camaro", ClientID = 4, ClientName = "Елена Синяя", ID = 3453, Status = "Completed" });
            res.Add(new OrderModel() { Car = "Волга 3110", ClientID = 6, ClientName = "Николай Мешков", ID = 345, Status = "Created" });
            rr.Page = res.ToArray();
            rr.PageCount = 1;
            
            return rr;
        }

    }
}
