﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Models
{/// <summary>
 /// Summary description for OrderModel
 /// </summary>
    public class OrderModel
    {
        public int ID { get; set; }
        public int ClientID { get; set; }
        public string ClientName { get; set; }
        public string Car { get; set; }
        public string Status { get; set; }
    }
}