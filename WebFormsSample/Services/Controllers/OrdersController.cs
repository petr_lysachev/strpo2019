﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using DBLayer;
using Models;
using System.Data.Entity.Infrastructure;
using BLInterfaces;

namespace Services.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class OrdersController : ApiController
    {
        IOrdersService _service;
        public OrdersController(IOrdersService service)
        {
            _service = service;
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        // GET api/orders
        public HttpResponseMessage Get(int page = 1, int pageLen = 10, string sortBy = "", string sort = "")
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, 
                    _service.FindMany(page, pageLen, sortBy, sort), 
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
#if DEBUG
                    ex.ToString()
#else
                    "Server error occured"
#endif
                    );
            }
        }

        // GET api/orders/5
        public string Get(int id)
        {
            return "order";
        }

        // POST api/orders
        public void Post([FromBody]string order)
        {
        }

        // PUT api/orders/5
        public void Put(int id, [FromBody]string order)
        {
        }

        // DELETE api/orders/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                using (var context = new Strpo2019_RusEntities())
                {
                    var o = (from c in context.Orders where c.ID == id select c).FirstOrDefault();
                    if (o == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound);
                    }
                    context.Orders.Remove(o);
                    context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (DbUpdateException dbex)
            {
                return Request.CreateResponse(HttpStatusCode.Conflict,
#if DEBUG
                    dbex.ToString()
#else
                    "There are some entities in the database that should be deleted first"
#endif
                    );
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
#if DEBUG
                    ex.ToString()
#else
                    "Server error occured"
#endif
                    );
            }
            }
        }
}
