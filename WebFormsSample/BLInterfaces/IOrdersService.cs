﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLInterfaces
{
    public interface IOrdersService
    {
        PagedResult<OrderModel> FindMany(int page = 1, int pageLen = 10, string sortBy = "", string sort = "");
    }
}
