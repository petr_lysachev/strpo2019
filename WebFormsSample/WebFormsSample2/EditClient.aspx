﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditClient.aspx.cs" Inherits="EditClient" %>
<%@ Register Src="~/Controls/ClientControl.ascx" TagName="ClientControl" TagPrefix="fkn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <fkn:ClientControl ID="clientEditor" runat="server"/>

        <asp:Button runat="server" Text="Save" ID="btnSave"/>
        <asp:Button runat="server" Text="Cancel" ID="btnCancel"/>
    </form>
</body>
</html>
