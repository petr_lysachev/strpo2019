﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DBLayer;
using Models;

public partial class Controls_ClientControl : System.Web.UI.UserControl
{
    public int ClientID
    {
        get;
        set;
    }

    public string Name 
    { 
        get { return txtName.Text;  } 
        set { txtName.Text = value;  }
    }

    public string Surname
    {
        get { return txtSurname.Text; }
        set { txtSurname.Text = value; }
    }

    public string Phone
    {
        get { return txtPhone.Text; }
        set { txtPhone.Text = value; }
    }

    bool EditMode
    {
        get
        {
            return ClientID != 0;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (EditMode) 
                fillData();
        }
    }

    private void fillData()
    {
        using (var context = new Strpo2019_RusEntities())
        {
            var client = (from c in context.Clients where c.ID == ClientID select c).First();
            txtName.Text = client.Name;
            txtSurname.Text = client.Surname;
            txtPhone.Text = client.Phone;
            context.SaveChanges();

        }
    }

}