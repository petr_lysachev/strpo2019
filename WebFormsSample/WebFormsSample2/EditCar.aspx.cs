﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DBLayer;
using Models;

public partial class EditCar : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        txtYear.MaxYear = DateTime.Now.Year;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        btnSave.Click += BtnSave_Click;
        btnCancel.Click += BtnCancel_Click;

        if (!IsPostBack)
            loadData();
    }

    private void loadData()
    {

        using (var context = new Strpo2019_RusEntities())
        {
            ddlMake.DataSource = context.CarMakes.ToList();
            ddlMake.DataBind();

            var client = (from c in context.Clients where c.ID == ID select c).First();

            ddlMake.SelectedValue = client.CarMakeID.ToString();
            txtModel.Text = client.Model;
            if (client.CarYear.HasValue) 
                txtYear.Year = client.CarYear.Value;
            context.SaveChanges();
            
        }
    }

    private void BtnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainPage.aspx");
    }

    public int ID
    {
        get
        {
            return int.Parse(Request.Params["ID"]);
        }
    }

    private void BtnSave_Click(object sender, EventArgs e)
    {
        using (var context = new Strpo2019_RusEntities())
        {
            var client = (from c in context.Clients where c.ID == ID select c).First();
            client.CarMakeID = int.Parse(ddlMake.SelectedValue);
            client.Model = txtModel.Text;
            client.CarYear = txtYear.Year;
            context.SaveChanges();
            Response.Redirect("MainPage.aspx");
        }
    }
}