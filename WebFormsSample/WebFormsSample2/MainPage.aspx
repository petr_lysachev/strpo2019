﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MainPage.aspx.cs" Inherits="MainPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Заказы</title>

</head>
<body>
    <form id="form1" runat="server">
        <button id="btnAdd" runat="server">Add New Client</button>
        <button id="btnOrders" runat="server">Orders (jQuery)</button>
        <h1>Clients</h1>
            <asp:Panel ID="errPanel" runat="server" ForeColor="Red" Visible="false">
                <asp:Label ID="lblMsg" runat="server"></asp:Label>
                <asp:Label ID="lblDetails" runat="server"></asp:Label>
                <asp:Button ID ="btnDelAll" visible="false" runat="server" Text="Удалить вместе с заказами"/>
            </asp:Panel>
            <asp:DataGrid ID="dgClients" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundColumn DataField="FirstName" HeaderText="Имя"></asp:BoundColumn>
                    <asp:BoundColumn DataField="LastName"  HeaderText="Фамилия"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Phone" HeaderText="Телефон"></asp:BoundColumn>
                    <asp:ButtonColumn Text="Edit" CommandName="Edit"></asp:ButtonColumn>
                    <asp:ButtonColumn Text="Delete" CommandName="Delete"></asp:ButtonColumn>
                </Columns>
            </asp:DataGrid>

        <h1>Orders</h1>
		<asp:Repeater ID="rpt" runat="server">
			<HeaderTemplate>
	<table>
		<thead>
			<tr>
				<th>
					Заказ
				</th>
				<th>
					Клиент
				</th>
				<th>
					Авто
				</th>
				<th>
					Статус
				</th>
				<th>
					
				</th>
			</tr>
		</thead>
		<tbody>
				
			</HeaderTemplate>
			<ItemTemplate>
			<tr>
				<td>
					<%# DataBinder.Eval(Container.DataItem, "ID") %>
				</td>
				<td>
					<%# DataBinder.Eval(Container.DataItem, "ClientName") %>
				</td>
				<td>
					<%# DataBinder.Eval(Container.DataItem, "Car") %>
				</td>
				<td>
					<%# DataBinder.Eval(Container.DataItem, "Status") %>
				</td>
				<td>
                    <a href="EditClient.aspx?ID=<%# DataBinder.Eval(Container.DataItem, "ClientID") %>">Edit Client</a>					
				</td>
				<td>
                    <a href="EditCar.aspx?ID=<%# DataBinder.Eval(Container.DataItem, "ClientID") %>">Edit Car</a>					
				</td>
			</tr>
			</ItemTemplate>
			<FooterTemplate>
		</tbody>
	</table>
			</FooterTemplate>

		</asp:Repeater>
    </form>
</body>
</html>
