﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DBLayer;
using Models;

public partial class EditClient : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        btnSave.Click += BtnSave_Click;
        btnCancel.Click += BtnCancel_Click;
        clientEditor.ClientID = ID;
    }

    private void BtnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainPage.aspx");
    }

    public int ID
    {
        get
        {
            return int.Parse(Request.Params["ID"]);
        }
    }

    private void BtnSave_Click(object sender, EventArgs e)
    {
        using (var context = new Strpo2019_RusEntities())
        {
            var client = (from c in context.Clients where c.ID == ID select c).First();
            client.Name = clientEditor.Name;
            client.Surname = clientEditor.Surname;
            client.Phone = clientEditor.Phone;
            context.SaveChanges();
            Response.Redirect("MainPage.aspx");
        }
    }
}