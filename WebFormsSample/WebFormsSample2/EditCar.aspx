﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditCar.aspx.cs" Inherits="EditCar" %>
<%@ Register Assembly="CFSControls"  Namespace="CFSControls.Editors"  TagPrefix="csf" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/jquery-3.4.1.min.js" type="text/javascript" ></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server" Text="Make (Producer)"></asp:Label>
            <asp:DropDownList runat="server" ID ="ddlMake" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
        </div>
        <div>
            <asp:Label runat="server" Text="Model"></asp:Label>
            <asp:TextBox runat="server" ID ="txtModel"></asp:TextBox>
        </div>
        <div>
            <asp:Label runat="server" Text="Year"></asp:Label>
            <csf:YearTextBox runat="server" ID ="txtYear" MinYear="1901"></csf:YearTextBox>
        </div>
        <script type="text/javascript">
            $("#<%=txtYear.ClientID%>").on("change", function (e) { console.log(e);});
        </script>

        <asp:Button runat="server" Text="Save" ID="btnSave"/>
        <asp:Button runat="server" Text="Cancel" ID="btnCancel"/>
    </form>
</body>
</html>
