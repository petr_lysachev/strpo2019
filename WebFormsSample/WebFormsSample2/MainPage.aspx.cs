﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity.Infrastructure;
using DBLayer;
using Models;

public partial class MainPage : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		fillOrders();
        fillClients();
        btnAdd.ServerClick += BtnAdd_ServerClick;
        dgClients.DeleteCommand += DgClients_DeleteCommand;
        btnDelAll.Click += BtnDelAll_Click;
        btnOrders.ServerClick += BtnOrders_ServerClick;

    }

    private void BtnOrders_ServerClick(object sender, EventArgs e)
    {
        Response.Redirect("Orders.aspx");
    }

    private void BtnDelAll_Click(object sender, EventArgs e)
    {
        if (ViewState["VictimID"] == null)
            return;
#warning todo: add error handling
        int id = (int)ViewState["VictimID"];
#warning todo: add error handling

        if (DeleteClient(id, true))
            Response.Redirect("MainPage.aspx");
    }

    private void DgClients_DeleteCommand(object source, DataGridCommandEventArgs e)
    {
        var o = e.Item.ID;
        int id = 0;
        var clients = (dgClients.DataSource as List<ClientModel>);
        try
        {
            if (clients != null)
            {
                id = clients[e.Item.ItemIndex].ID;
                if (DeleteClient(id, false))
                    Response.Redirect("MainPage.aspx");
            }
        }        
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
            errPanel.Visible = true;
        }
    }

    private bool DeleteClient(int id, bool delOrders)
    {
        errPanel.Visible = false;
        btnDelAll.Visible = false;

        try
        {
            using (var context = new Strpo2019_RusEntities())
            {
                if (delOrders)
                {
                    context.Orders.RemoveRange((from o in context.Orders
                                                where o.CliendID == id
                                                select o));
                }                
                
                context.Clients.Remove((from cc in context.Clients
                                        where cc.ID == id
                                        select cc).First());
                context.SaveChanges();
            }
        }
        catch (DbUpdateException dbex)
        {
            var es = dbex.Entries.ToArray();
            if (es.Length > 0)
            {
                var obj = es[0].Entity;
                var property = obj.GetType().GetProperty("Surname");
                var surname = property.GetValue(obj, null);
                lblDetails.Text = surname + " не хочет удаляться";
                if (!delOrders)
                {
                    btnDelAll.Visible = true;
                    ViewState["VictimID"] = id;
                }
            }
            else
            {
                lblDetails.Text = "Ошибка удаления из базы данных";
            }
            errPanel.Visible = true;
            return false;
        }
        return true;
    }

    private void fillClients()
    {
        using (var context = new Strpo2019_RusEntities())
        {
            dgClients.DataSource = (from c in context.Clients
             select new ClientModel()
             {
                 FirstName = c.Name,
                 LastName = c.Surname,
                 Phone = c.Phone,
                 ID = c.ID
             }).ToList();
            dgClients.DataBind();
        }

    }

    private void BtnAdd_ServerClick(object sender, EventArgs e)
    {
        Response.Redirect("CreateClient.aspx");
    }

    void fillOrders()
	{
		using (var context = new Strpo2019_RusEntities())
		{
			var data = (from o in context.Orders
			 select
				new OrderModel()
				{
				ID = o.ID,
                ClientID = o.CliendID,
				ClientName = o.Client.Name + " " + o.Client.Surname,
				Car = o.Client.CarMake.Name + " " + o.Client.Model + " " + o.Client.CarYear.ToString(),
				Status = o.Status.Name
				}).ToArray();

			rpt.DataSource = data;
			rpt.DataBind();
		}
	}
}