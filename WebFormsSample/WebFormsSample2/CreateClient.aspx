﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CreateClient.aspx.cs" Inherits="CreateClient" %>
<%@ Register Src="~/Controls/ClientControl.ascx" TagName="ClientControl" TagPrefix="fkn" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <fkn:ClientControl ID="clientEditor" runat="server"/>
        <hr />
        <div>
            <asp:Label runat="server" Text="Car Make"></asp:Label>
            <asp:DropDownList runat="server" ID ="ddlMake" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
        </div>
        <div>
            <asp:Label runat="server" Text="Model"></asp:Label>
            <asp:TextBox runat="server" ID ="txtModel"></asp:TextBox>
        </div>
        <div>
            <asp:Label runat="server" Text="Year"></asp:Label>
            <asp:TextBox runat="server" ID ="txtYear"></asp:TextBox>
        </div>
        <asp:Button runat="server" Text="Save" ID="btnSave"/>
        <asp:Button runat="server" Text="Cancel" ID="btnCancel"/>
    </form>
</body>
</html>
