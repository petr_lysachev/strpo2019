﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DBLayer;
using Models;

public partial class CreateClient : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        btnSave.Click += BtnSave_Click;
        btnCancel.Click += BtnCancel_Click;
        fillData();
    }

    private void fillData()
    {
        using (var context = new Strpo2019_RusEntities())
        {
            ddlMake.DataSource = context.CarMakes.ToList();
            ddlMake.DataBind();
        }           
    }

    private void BtnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainPage.aspx");
    }

    private void BtnSave_Click(object sender, EventArgs e)
    {
        using (var context = new Strpo2019_RusEntities())
        {
            var client = new Client();
            client.Name = clientEditor.Name;
            client.Surname = clientEditor.Surname;
            client.Phone = clientEditor.Phone;
            client.CarMakeID = int.Parse(ddlMake.SelectedValue);
            client.CarYear = int.Parse(txtYear.Text);
            client.Model = txtModel.Text;

            context.Clients.Add(client);
            context.SaveChanges();
            Response.Redirect("MainPage.aspx");
        }
    }
}