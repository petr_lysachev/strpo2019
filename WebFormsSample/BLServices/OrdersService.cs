﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLInterfaces;
using DBLayer;
using Models;

namespace BLServices
{
    public class OrdersService : IOrdersService
    {
        Dictionary<string, Func<Order, string>> _sortFuncs;

        public OrdersService()
        {
            _sortFuncs = new Dictionary<string, Func<Order, string>>();
            _sortFuncs.Add("ClientName", o => o.Client.Surname + " " + o.Client.Name);
            _sortFuncs.Add("ID", o => o.ID.ToString());
            _sortFuncs.Add("StatusName", o => o.Status.Name);
            _sortFuncs.Add("Car", o => o.Client.CarMake.Name + " " + o.Client.Model);
        }

        public PagedResult<OrderModel> FindMany(int page = 1, int pageLen = 10, string sortBy = "", string sort = "")
        {
            using (var context = new Strpo2019_RusEntities())
            {
                IEnumerable<Order> query = null;
                switch (sort)
                {
                    case "asc":
                        query = (from o in context.Orders select o).OrderBy(_sortFuncs[sortBy]).Skip((page - 1) * pageLen).Take(pageLen);
                        break;
                    case "desc":
                        query = (from o in context.Orders select o).OrderByDescending(_sortFuncs[sortBy]).Skip((page - 1) * pageLen).Take(pageLen);
                        break;
                    default:
                        query = (from o in context.Orders select o).OrderBy(o => o.ID).Skip((page - 1) * pageLen).Take(pageLen);
                        break;
                }

                int pc = query.Count() / pageLen + 1;

                var dbEntities = query.ToArray();

                var data = (from o in dbEntities
                            select new OrderModel()
                            {
                                ID = o.ID,
                                ClientID = o.CliendID,
                                ClientName = o.Client.Name + " " + o.Client.Surname,
                                Car = o.Client.CarMake.Name + " " + o.Client.Model + " " + o.Client.CarYear.ToString(),
                                Status = o.Status.Name
                            }).ToArray();

                return new PagedResult<OrderModel>() { Page = data, PageCount = pc };
            }          
            
        }

    }
}
